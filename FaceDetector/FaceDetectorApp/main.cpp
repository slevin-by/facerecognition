#include <iostream>
#include "FaceDetect.h"

#include <boost\filesystem.hpp>

bool isImage(boost::filesystem::path &ext);
bool searchImages(const boost::filesystem::path &dirPath, std::vector<std::string> &files);

int main(int argc, char **argv)
{
	char *szPath = NULL;
	unsigned long nWorkers = 0;

	if (argc < 2)
	{
		printf("ERROR: Wrong amount of arguments.\n");
		printf("USAGE: %s <path_to_image> [<workers_number>]\n", argv[0]);
		return EXIT_FAILURE;
	}
	else
	{
		szPath = argv[1];
		nWorkers = argc > 2 
			? atol(argv[2])
			: 5;
	}

	std::vector<std::string> vFiles;
	if (!searchImages(szPath, vFiles))
	{
		printf("ERROR: There are no images in %s\n", szPath);
		return EXIT_FAILURE;
	}

	int nRet = detect_faces((void *)&vFiles, nWorkers);
	if (FD_BAD == nRet)
	{
		printf("ERROR: Can not load xml.\n");
		return EXIT_FAILURE;
	}

	printf(" *** %u faces have been recognized.\n", g_nRecognized);
	if (g_nRecognized)
		printf(" *** Results are in the: %s\n", boost::filesystem::current_path().string().c_str());

	return EXIT_SUCCESS;
}

bool isImage(boost::filesystem::path &ext)
{
	if (ext == ".jpg" || ext == ".png" || ext == ".bmp")
		return true;
	return false;
}

bool searchImages(const boost::filesystem::path &dirPath, std::vector<std::string> &files)
{
	using namespace boost::filesystem;

	if (!exists(dirPath) || !is_directory(dirPath))
		return false;

	recursive_directory_iterator dir(dirPath), end;
	while (dir != end)
	{
		if (is_regular_file(*dir) && isImage(dir->path().extension()))
			files.push_back(dir->path().generic_string());
		++dir;
	}
	return true;
}
