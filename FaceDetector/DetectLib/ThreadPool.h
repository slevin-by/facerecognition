#ifndef _THREADPOOL_H_
#define _THREADPOOL_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <deque>

class ThreadPool;

class WorkerThread
{
public:
	WorkerThread(ThreadPool &tp) 
		: m_tPool(tp) 
	{}
	void operator()();

private:
	ThreadPool &m_tPool;
};

class ThreadPool
{
	using worker_t = std::function<void(void *)>;
	using task_t = std::pair<worker_t, void *>;

public:
	ThreadPool(size_t size);
	~ThreadPool();

	bool isBusy()
	{
		std::unique_lock<std::mutex> lock(m_tMutex);
		if (!m_bIsStoped && !m_dqTasks.empty())
			return true;
		return false;
	}

	template<class TFunc>
	void add(TFunc pFunc, void *arg)
	{
		{
			std::unique_lock<std::mutex> lock(m_tMutex);
			m_dqTasks.emplace_back(pFunc, arg);
		}
		m_tCond.notify_one();
	}
	
private:
	friend class WorkerThread;

	std::vector<std::thread> m_vWorkers;
	std::deque<task_t> m_dqTasks;

	std::mutex m_tMutex;
	std::condition_variable m_tCond;
	volatile bool m_bIsStoped;

	size_t m_nSize;
};

#endif // _THREADPOOL_H_
