#include "stdafx.h"

#define FD_API extern "C" __declspec(dllexport)
#include "FaceDetect.h"

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "ThreadPool.h"
#include "JsonWriter.h"
#include "ConfigParser.h"

#include <chrono>
#include <ctime>
#include <map>
#include <memory>

#include <boost\filesystem.hpp>

#define HAAR_FACE_PATH	"..\\haarcascades\\haarcascade_frontalface_alt2.xml"

std::map<std::string, std::shared_ptr<CJsonWriter>> g_mJsonWriterMap;
CConfig g_cvConfig;

volatile int g_nRecognized;
std::mutex g_jsonMutex;
volatile static int g_nError;

static void process_face(void *arg);
std::string get_current_time();

FD_API int detect_faces(void *vPaths, unsigned long workers_number)
{
	std::vector<std::string> *rvPaths = (std::vector<std::string> *)vPaths;

	g_nRecognized = 0;
	g_nError = FD_OK;
	g_mJsonWriterMap.clear();

	boost::filesystem::path dir;
	for (auto &itv : *rvPaths)
	{
		dir = itv;
		dir.remove_filename();
		g_mJsonWriterMap[dir.string()] = std::make_shared<CJsonWriter>(CJsonWriter());
	}

	CConfigParser::ParseConfig(g_cvConfig);

	ThreadPool pool(workers_number);
	for (auto &itv : *rvPaths)
		pool.add(process_face, (void *)(itv.c_str()));

	while (true)
	{
		if (!pool.isBusy())
			break;
	}

	return g_nError;
}

static void process_face(void *arg)
{
	using namespace cv;

	const char *szPath = (const char *)arg;
	boost::filesystem::path tPath(szPath);
	std::string szDir = tPath.parent_path().string();
	std::string szFile = tPath.stem().string();
	std::string szExt = tPath.extension().string();

	std::shared_ptr<CJsonWriter> &rJsonWriter = g_mJsonWriterMap[szDir];

	CascadeClassifier ccFaceClassifier;
	std::vector<Rect> vFaces;
	Mat tImage;

	if (!ccFaceClassifier.load(HAAR_FACE_PATH))
	{
#ifdef _DEBUG
		std::thread::id tid = std::this_thread::get_id();
		fprintf(stderr, "ERROR: cv::CascadeClassifier::load fault. Thread: %u\n", tid);
#endif // _DEBUG
		g_nError = FD_BAD;
		return;
	}

	tImage = imread(szPath);

	Mat grayImage;
	cvtColor(tImage, grayImage, COLOR_BGR2GRAY);
	equalizeHist(grayImage, grayImage);

	ccFaceClassifier.detectMultiScale(grayImage, vFaces, 
		g_cvConfig.m_dScaleFactor, g_cvConfig.m_nMinNeighbours, 0 | CASCADE_SCALE_IMAGE, Size(30, 30));
	g_nRecognized += vFaces.size();

	Rect rect;
	Mat crop;
	std::string szFileName;
	for (size_t i = 0; i < vFaces.size(); i++)
	{
		rect.x = vFaces[i].x;
		rect.y = vFaces[i].y;
		rect.width = (vFaces[i].width);
		rect.height = (vFaces[i].height);

		crop = tImage(rect);

		std::stringstream ssFile;
		ssFile << szFile << "_" << "face" << i << "_" << get_current_time();
		szFileName = ssFile.str() + szExt;

		{
			std::lock_guard<std::mutex> lg(g_jsonMutex);
			rJsonWriter->addFrame(ssFile.str(), CPoint{ rect.x, rect.y }, CPoint{ rect.x + rect.width, rect.y + rect.height });
		}

		imwrite(szFileName, crop);
	}

	rJsonWriter->write(std::string(szDir + "/result.json").c_str());
}

std::string get_current_time()
{
	using namespace std::chrono;
	return std::to_string(system_clock::to_time_t(system_clock::now()));
}
