#include "JsonWriter.h"
#include <boost\property_tree\json_parser.hpp>

void CJsonWriter::addFrame(std::string path, CPoint p1, CPoint p2)
{
	using boost::property_tree::ptree;

	ptree frame;
	ptree row;
	ptree cell;

	cell.put_value(p1.x);
	row.push_back(std::make_pair("", cell));
	cell.clear();

	cell.put_value(p1.y);
	row.push_back(std::make_pair("", cell));
	cell.clear();

	frame.push_back(std::make_pair("", row));
	row.clear();

	cell.put_value(p2.x);
	row.push_back(std::make_pair("", cell));
	cell.clear();

	cell.put_value(p2.y);
	row.push_back(std::make_pair("", cell));
	cell.clear();

	frame.push_back(std::make_pair("", row));
	row.clear();

	m_root.add_child(path, frame);
}

void CJsonWriter::write(const char *path)
{
	using boost::property_tree::write_json;

	write_json(path, m_root);
}

void CJsonWriter::clear()
{
	using boost::property_tree::ptree;

	ptree tree;
	m_root.swap(tree);
}
