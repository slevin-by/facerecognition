#ifndef _CONFIGPARSER_H_
#define _CONFIGPARSER_H_

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#define INI_PATH ".\\"
#define INI_NAME "config.ini"

#define SCALE_FACTOR_DEFAULT	((double)1.2)
#define MIN_NEIGHBOURS_DEFAULT	((int)3)

#define CONFIG_SCALE_FACTOR		"scaleFactor"
#define CONFIG_MIN_NEIGHBOURS	"minNeighbours"

struct CConfig
{
	double m_dScaleFactor;
	int m_nMinNeighbours;
};

class CConfigParser
{
public:
	CConfigParser()
	{
		using namespace boost::filesystem;
		if (exists(INI_PATH INI_NAME) && is_regular_file(INI_PATH INI_NAME))
			boost::property_tree::ini_parser::read_ini(INI_NAME, m_root);
	}

	bool isCorrect() const
	{
		return !m_root.empty();
	}

	template<typename T>
	bool getConfig(const char *szConfig, T &ret)
	{
		if (m_root.find(szConfig) == m_root.not_found())
			return false;
		ret = m_root.get<T>(szConfig);
		return true;
	}

	static void ParseConfig(CConfig &config)
	{
		CConfigParser parser;
		if (parser.isCorrect())
		{
			if (!parser.getConfig(CONFIG_SCALE_FACTOR, config.m_dScaleFactor))
				config.m_dScaleFactor = SCALE_FACTOR_DEFAULT;
			if (!parser.getConfig(CONFIG_MIN_NEIGHBOURS, config.m_nMinNeighbours))
				config.m_nMinNeighbours = MIN_NEIGHBOURS_DEFAULT;
		}
		else
		{
			config.m_dScaleFactor = SCALE_FACTOR_DEFAULT;
			config.m_nMinNeighbours = MIN_NEIGHBOURS_DEFAULT;
		}
	}

private:
	boost::property_tree::ptree m_root;
};

#endif // _CONFIGPARSER_H_
