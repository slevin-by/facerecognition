#ifndef _JSONWRITER_H_
#define _JSONWRITER_H_

#include <boost\property_tree\ptree.hpp>

struct CPoint
{
	int x;
	int y;
};

class CJsonWriter
{
public:
	void addFrame(std::string path, CPoint p1, CPoint p2);
	void write(const char *path);
	void clear();

private:
	boost::property_tree::ptree m_root;
};

#endif // _JSONWRITER_H_
