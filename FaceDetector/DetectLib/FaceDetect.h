#ifndef _FACEDETECT_H_
#define _FACEDETECT_H_

#ifndef FD_API
#define FD_API extern "C" __declspec(dllimport)
#endif // FD_API

#define FD_OK	0x00000000
#define FD_BAD	0xFFFFFFFF

FD_API volatile int g_nRecognized; // Workaround: Yes, such export is ugly. But it always works proply.

FD_API int detect_faces(void *vPaths, unsigned long workers_number);

#endif // _FACEDETECT_H_
