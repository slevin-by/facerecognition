#include "ThreadPool.h"

#define MIN_THREAD_NUMBER	10
#define MAX_THREAD_NUMBER	20

ThreadPool::ThreadPool(size_t size = 15)
	: m_bIsStoped(false)
{
	m_nSize = (size < MIN_THREAD_NUMBER)
		? MIN_THREAD_NUMBER
		: (size > MAX_THREAD_NUMBER)
			? MAX_THREAD_NUMBER
			: size;
	for (size_t i = 0; i < m_nSize; i++)
		m_vWorkers.push_back(std::thread(WorkerThread(*this)));
}

ThreadPool::~ThreadPool()
{
	m_bIsStoped = true;
	m_tCond.notify_all();
	for (auto &itv : m_vWorkers)
		itv.join();
}

void WorkerThread::operator()()
{
	ThreadPool::task_t task;
	while (true)
	{
		{
			std::unique_lock<std::mutex> lock(m_tPool.m_tMutex);

			while (!m_tPool.m_bIsStoped && m_tPool.m_dqTasks.empty())
				m_tPool.m_tCond.wait(lock);

			if (m_tPool.m_bIsStoped)
				return;

			task = m_tPool.m_dqTasks.front();
			m_tPool.m_dqTasks.pop_front();
		}
		task.first(task.second);
	}
}
